﻿// -----------------------------------------------------------------------
// <copyright file="FileIconCache.cs">
//     Copyright (C) 2015 Stefan Hoffmann - All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MyDMS.Interop;
using FileInfo = MyDMS.Interop.FileInfo;

namespace MyDMS
{
    /// <summary>
    /// A file icon cache.
    /// </summary>
    internal class FileIconCache
    {

        private static readonly Dictionary<string, ImageSource> _iconCache = new Dictionary<string, ImageSource>();
        private static readonly string[] _noCacheExtensions = {".exe"};

        /// <summary>
        /// Returns the file icon associated with a file. If the icon is not yet cached, it will be retrieved automatically.
        /// </summary>
        /// <param name="filename">The file.</param>
        /// <returns>The icon or null if no icon could be found.</returns>
        public static ImageSource GetFileIcon(string filename)
        {
            if (string.IsNullOrEmpty(filename)) throw new ArgumentNullException();
            if (!File.Exists(filename)) throw new FileNotFoundException();

            string extension = Path.GetExtension(filename).ToLower();
            ImageSource result;

            lock (_iconCache)
            {
                if (_iconCache.TryGetValue(extension, out result)) return result;

                try {
                    FileInfo.SHFILEINFO shinfo = new FileInfo.SHFILEINFO();
                    const FileInfo.SHGFI flags = FileInfo.SHGFI.ICON | FileInfo.SHGFI.USEFILEATTRIBUTES |
                                        FileInfo.SHGFI.LARGEICON;

                    // every time SHGetFileInfo gets called from a MTA thread a kitten dies
                    // or you may just get strange results
                    if (Thread.CurrentThread.GetApartmentState() == ApartmentState.STA) {
                        FileInfo.SHGetFileInfo(filename, FILE_ATTRIBUTE.NORMAL, ref shinfo,
                            (uint)Marshal.SizeOf(shinfo),
                            flags);
                    } else
                    {
                        StaTask.Run(
                            () =>
                                FileInfo.SHGetFileInfo(filename, FILE_ATTRIBUTE.NORMAL, ref shinfo,
                                    (uint) Marshal.SizeOf(shinfo), flags)).Wait();
                    }

                    result =
                        Imaging.CreateBitmapSourceFromHIcon(shinfo.hIcon,
                            Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                    result.Freeze();
                    FileInfo.DestroyIcon(shinfo.hIcon);
                } catch {
                    result = null;
                }
                if (_noCacheExtensions.Contains(extension)) return result;
                return _iconCache[extension] = result;
            }
        }

        /// <summary>
        /// Returns the icon associated with a specific folder. Results are not cached.
        /// </summary>
        /// <param name="path">Foldername including full path.</param>
        /// <returns>The icon or null if no icon could be found.</returns>
        public static ImageSource GetFolderIcon(string path)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException();
            if (!Directory.Exists(path)) throw new DirectoryNotFoundException();

            try {
                FileInfo.SHFILEINFO shinfo = new FileInfo.SHFILEINFO();
                const FileInfo.SHGFI flags = FileInfo.SHGFI.ICON | FileInfo.SHGFI.SMALLICON;
                
                // every time SHGetFileInfo gets called from a MTA thread a kitten dies
                // or you may just get strange results
                if (Thread.CurrentThread.GetApartmentState() == ApartmentState.STA)
                {
                    FileInfo.SHGetFileInfo(path, 0, ref shinfo, (uint) Marshal.SizeOf(shinfo), flags);
                } else
                {
                    StaTask.Run(() => FileInfo.SHGetFileInfo(path, 0, ref shinfo, (uint) Marshal.SizeOf(shinfo), flags))
                        .Wait();
                }

                var image = Imaging.CreateBitmapSourceFromHIcon(shinfo.hIcon, Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());
                image.Freeze();
                FileInfo.DestroyIcon(shinfo.hIcon);
                return image;
            } catch {
                return null;
            }
        }
    }
}