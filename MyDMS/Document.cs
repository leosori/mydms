// -----------------------------------------------------------------------
// <copyright file="Document.cs">
//     Copyright (C) 2015 Stefan Hoffmann - All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MyDMS
{
    public class Document
    {
        private string _content;
        private string _absoluteFilePath;
        private string _rootFolderPath;
        public string Name { get; set; }
        public string Folder { get; set; }
        public DateTime? DocumentDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Filename { get; set; }
        public string Tag { get; set; }

        public string Content
        {
            get
            {
                if (_content == null)
                {
                    _content = FileIndexer.ExtractTextFromFile(_absoluteFilePath);
                }
                return _content;
            }
            set { _content = value; }
        }

        public ImageSource FileIcon { get; private set; }

        public Document()
        {
        }

        public Document(string filename, string rootFolderPath, bool initAllProperties = false)
        {
            if (filename == null) throw new ArgumentNullException("filename");

            _rootFolderPath = rootFolderPath;
            if (Path.IsPathRooted(filename))
            {
                _absoluteFilePath = filename;
                Filename = DocumentService.GetRelativePath(filename, _rootFolderPath);
            }
            else
            {
                Filename = filename;
                _absoluteFilePath = DocumentService.GetAbsolutePath(filename, _rootFolderPath);
            }

            if (!DeriveMetadataFromFilename(_absoluteFilePath))
            {
                Name = Path.GetFileNameWithoutExtension(_absoluteFilePath);
            }

            if (initAllProperties)
            {
                Content = FileIndexer.ExtractTextFromFile(_absoluteFilePath);
            }

            FileIcon = FileIconCache.GetFileIcon(_absoluteFilePath);
            Folder = Path.GetDirectoryName(Filename);
            Tag = string.Empty;
            ModifiedDate = File.GetLastWriteTime(_absoluteFilePath);
        }

        /// <summary>
        /// Create a new Document based on a file. All properties will be initialized.
        /// </summary>
        /// <param name="filename">The absolute or relative filepath.</param>
        /// <param name="rootFolderPath">The root folder path.</param>
        /// <returns>The Document.</returns>
        public static async Task<Document> CreateDocumentAsync(string filename, string rootFolderPath)
        {
            if (filename == null) throw new ArgumentNullException("filename");

            var doc = new Document();

            doc._rootFolderPath = rootFolderPath;
            if (Path.IsPathRooted(filename))
            {
                doc._absoluteFilePath = filename;
                doc.Filename = DocumentService.GetRelativePath(filename, rootFolderPath);
            }
            else
            {
                doc.Filename = filename;
                doc._absoluteFilePath = DocumentService.GetAbsolutePath(filename, rootFolderPath);
            }

            if (!doc.DeriveMetadataFromFilename(doc._absoluteFilePath))
            {
                doc.Name = Path.GetFileNameWithoutExtension(doc._absoluteFilePath);
            }
            var contentTask = FileIndexer.ExtractTextFromFileAsync(doc._absoluteFilePath).ConfigureAwait(false);
            doc.FileIcon = FileIconCache.GetFileIcon(doc._absoluteFilePath); //TODO make async?
            doc.Folder = Path.GetDirectoryName(doc.Filename);
            doc.Tag = string.Empty;
            doc.ModifiedDate = File.GetLastWriteTime(doc._absoluteFilePath);
            doc.Content = await contentTask;

            return doc;
        }

        private bool DeriveMetadataFromFilename(string filename)
        {
            // Beispiel "Dokumente\2015-05-01 Mein Dokument.pdf"
            //          "Dokumente\2015-05-01 1637 Mein Dokument.pdf"
            var match = Regex.Match(filename, @"^.*\\?(?<Date>\d{4}-\d{2}-\d{2})\s+((?<Time>\d{4})\s+)?(?<Name>.+)\..+");
            if (!match.Success) return false;

            Name = match.Groups["Name"].Value;

            try
            {
                if (match.Groups["Time"].Success)
                {
                    var s = match.Groups["Date"].Value + " " + match.Groups["Time"].Value;
                    DocumentDate = DateTime.ParseExact(s, "yyyy-MM-dd HHmm", CultureInfo.InvariantCulture);
                }
                else
                {
                    DocumentDate = DateTime.ParseExact(match.Groups["Date"].Value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                }
            }
            catch (Exception)
            {
                DocumentDate = null;
            }
            return true;
        }

        public override string ToString()
        {
            return "[" + Folder + "] " + Name;
        }

        /// <summary>
        /// Populates properties that are not returned by a database query.
        /// </summary>
        /// <remarks>
        /// The database does not save properties like FileIcon or Folder. This method will complete the missing information.
        /// </remarks>
        /// <param name="rootFolderPath">The root folder path.</param>
        /// <returns>The updated Document.</returns>
        public Document CompleteDatabaseDocument(string rootFolderPath)
        {
            _rootFolderPath = rootFolderPath;
            _absoluteFilePath = DocumentService.GetAbsolutePath(Filename, _rootFolderPath);
            FileIcon = FileIconCache.GetFileIcon(_absoluteFilePath);
            Folder = Path.GetDirectoryName(Filename);
            return this;
        }
    }
}