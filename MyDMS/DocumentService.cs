﻿// -----------------------------------------------------------------------
// <copyright file="DocumentService.cs">
//     Copyright (C) 2015 Stefan Hoffmann - All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MyDMS.SearchEngine;

namespace MyDMS
{
    public class DocumentService
    {
        private string _rootFolderPath;

        public string RootFolderPath
        {
            get
            {
                return _rootFolderPath;
            }
            set
            {
                // Sideeffect: 1) Converts Path.AltDirectorySeparatorChar to Path.DirectorySeparatorChar
                //             2) Checks if it's a valid path. The path must not exist.
                value = Path.GetFullPath(value);

                var i = value.Length - 1; 
                var firstSeparator = value.IndexOf(Path.DirectorySeparatorChar);
                while (value[i] == Path.DirectorySeparatorChar && i > firstSeparator) i--;
                _rootFolderPath = value.Substring(0, i+1);
            }
        }

        private readonly LuceneEngine _lucene;

        public DocumentService(LuceneEngine lucene)
        {
            if (lucene == null) throw new ArgumentNullException("lucene");
            _lucene = lucene;
        }

        public IEnumerable<Document> ScanFolder(string folderPath, SearchOption searchOption)
        {
            IEnumerable<string> fileList = Directory.EnumerateFiles(folderPath, "*", searchOption);
            return fileList.AsParallel().Select(file =>
            {
                //TODO check and mark if file's last modified date differs from the indexed date
                var doc = _lucene.GetDocument(GetRelativePath(file, _rootFolderPath));
                if (doc != null)
                {
                    doc.CompleteDatabaseDocument(_rootFolderPath);
                    return doc;
                }
                return new Document(file, _rootFolderPath);
            }).OrderBy(doc => doc.DocumentDate);
        }

        public async Task<IEnumerable<Document>> ScanFolderAsync(string folderPath, SearchOption searchOption)
        {
            var result = await Task.Run(() => ScanFolder(folderPath, searchOption).ToList()).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Updates the index with all files in the base folder and its subfolders.
        /// </summary>
        /// <param name="progress">Receives progress updates from 0 to 100.</param>
        public void UpdateIndex(IProgress<int> progress = null)
        {
            if (RootFolderPath == null) return;

            var sw = Stopwatch.StartNew();
            var files = Directory.EnumerateFiles(RootFolderPath, "*", SearchOption.AllDirectories).Select(absolutePath => GetRelativePath(absolutePath, RootFolderPath));
            var filesToProcess = _lucene.GetNonIndexedFiles(files);
            int total = filesToProcess.Count;
            int counter = 0;
            Debug.WriteLine("need to add {0} files. Scanning needed {1} ms", total, sw.ElapsedMilliseconds);
            sw.Restart();
            _lucene.AddDocuments(filesToProcess.AsParallel().Select(file =>
            {
                if (progress != null)
                {
                    progress.Report(Interlocked.Increment(ref counter) * 100 / total);
                }
                return new Document(file, _rootFolderPath, true);
            }));
            sw.Stop();
            Debug.WriteLine("Needed {0} ms to update the index.", sw.ElapsedMilliseconds);
        }

        public static string GetRelativePath(string absolutePath, string rootFolderPath)
        {
            Debug.Assert(absolutePath.StartsWith(rootFolderPath, StringComparison.InvariantCultureIgnoreCase), "absolutePath doesn't start with RootFolderPath");

            string relativePath = absolutePath.Remove(0, rootFolderPath.Length);
            if (relativePath.Length > 0 && relativePath[0] == Path.DirectorySeparatorChar)
            {
                relativePath = relativePath.Remove(0, 1);
            }
            return relativePath;
        }

        public static string GetAbsolutePath(string relativePath, string rootFolderPath)
        {
            Debug.Assert(!Path.IsPathRooted(relativePath), "Path is rooted");
            return rootFolderPath + Path.DirectorySeparatorChar + relativePath;
        }

        public IEnumerable<Document> SearchIndex(string query) {
            IEnumerable<Document> searchResults = String.IsNullOrEmpty(query)
                ? _lucene.GetAllIndexedDocuments()
                : _lucene.RawSearch(query);
            searchResults = searchResults.Select(doc => doc.CompleteDatabaseDocument(RootFolderPath));
            return searchResults;
        }
    }
}