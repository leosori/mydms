﻿// -----------------------------------------------------------------------
// <copyright file="DirInfo.cs">
//     Copyright (C) 2015 Stefan Hoffmann - All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Media;

namespace MyDMS
{
    internal class DirInfo
    {
        private IEnumerable<DirInfo> _children;
        private ImageSource _icon;
        public string Name { get; set; }

        public string FullPath { get; private set; }

        public ImageSource Icon
        {
            get
            {
                if (_icon == null)
                {
                    _icon = FileIconCache.GetFolderIcon(FullPath);
                }
                return _icon;
            }
        }

        public IEnumerable<DirInfo> Children
        {
            get
            {
                if (_children == null)
                {
                    Debug.WriteLine("getting children from " + Name);
                    try
                    {
                        _children = Directory.EnumerateDirectories(FullPath, "*").Select(dir => new DirInfo(dir));
                    }
                    catch
                    {
                        _children = Enumerable.Empty<DirInfo>();
                    }
                }
                return _children;
            }
            private set { _children = value; }
        }

        public DirInfo(string path) {
            FullPath = path;
            // GetDirectory would return the parent directory
            Name = Path.GetFileName(path);
            // Root Path has no "filename"
            if (string.IsNullOrEmpty(Name)) Name = path;
        }

    }
}