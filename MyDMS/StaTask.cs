﻿// -----------------------------------------------------------------------
// <copyright file="StaTask.cs">
//     Copyright (C) 2015 Stefan Hoffmann - All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Schedulers;

namespace MyDMS
{
    /// <summary>
    /// Helper class to provide Tasks running within STA threads.
    /// </summary>
    internal static class StaTask
    {
        /// <summary>
        /// A TaskScheduler with STA threads.
        /// </summary>
        public static readonly TaskScheduler Scheduler = new StaTaskScheduler(Environment.ProcessorCount);
        
        /// <summary>
        /// A TaskFactory to create Tasks backed by STA threads.
        /// </summary>
        public static readonly TaskFactory Factory;

        static StaTask()
        {
            Factory = new TaskFactory(CancellationToken.None, TaskCreationOptions.DenyChildAttach,
                TaskContinuationOptions.None, Scheduler);
        }

        public static Task Run(Action action)
        {
            return Factory.StartNew(action);
        }

        public static Task<TResult> Run<TResult>(Func<TResult> function)
        {
            return Factory.StartNew(function);
        }
    }
}