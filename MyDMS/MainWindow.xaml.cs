﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyDMS.SearchEngine;

namespace MyDMS {
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IEnumerable<Document> _files;
        private readonly LuceneEngine _luceneEngine = new LuceneEngine();
        private readonly DocumentService _documentService;

        public MainWindow() {
            InitializeComponent();
            Trace.Listeners.Add(new UITraceListener(StatusText, true));
            _documentService = new DocumentService(_luceneEngine);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new System.Windows.Forms.FolderBrowserDialog { ShowNewFolderButton = false };
            var result = dlg.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                _documentService.RootFolderPath = dlg.SelectedPath;

                var rootDir = new DirInfo(_documentService.RootFolderPath);
                NavigationTree.Items[0] = rootDir;
                var container =
                    NavigationTree.ItemContainerGenerator.ContainerFromItem(rootDir) as TreeViewItem;
                container.IsSelected = true;
                container.IsExpanded = true;
            }
        }

        private async void Button_Click_CreateIndex(object sender, RoutedEventArgs e) {
            Debug.Assert(sender is Button);
            (sender as Button).IsEnabled = false;
            pbBackground.Value = 0;
            var progress = new Progress<int>(i => pbBackground.Value = i);
            Trace.WriteLine("Aktualisiere Index...");
            await Task.Run(() => _documentService.UpdateIndex(progress));
            Trace.WriteLine("Index aktualisiert.");
            (sender as Button).IsEnabled = true;
        }

        private void Button_Click_Search(object sender, RoutedEventArgs e)
        {
            SearchTreeViewItem.IsSelected = false;
            SearchTreeViewItem.DataContext = _documentService.SearchIndex(TxtSearchQuery.Text);
            SearchTreeViewItem.IsSelected = true;
        }

        private void lbDocs_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = lbDocs.SelectedItem as Document;
            if (item == null) return;
            try
            {
                // open file with standard application
                Process.Start(item.Filename);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fehler beim Öffnen des Dokuments", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private async void NavigationTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var dirInfo = NavigationTree.SelectedItem as DirInfo;
            _files = null;
            if (dirInfo != null)
            {
                _files = await _documentService.ScanFolderAsync(dirInfo.FullPath, SearchOption.TopDirectoryOnly);
            }
            else if (NavigationTree.SelectedItem == SearchTreeViewItem)
            {
                _files = (NavigationTree.SelectedItem as TreeViewItem).DataContext as IEnumerable<Document>;
            }
            lbDocs.ItemsSource = _files;
        }

        private void lbDocs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var doc = lbDocs.SelectedItem as Document;
            if (doc == null) return;
            _luceneEngine.ContainsDocument(doc.Filename);
        }

        private void ButtonSaveEdits_Click(object sender, RoutedEventArgs e)
        {
            // Update the Document
            var binding = txtName.GetBindingExpression(TextBox.TextProperty);
            if (binding != null) binding.UpdateSource();
            binding = txtTag.GetBindingExpression(TextBox.TextProperty);
            if (binding != null) binding.UpdateSource();

            // Update the index
            var doc = lbDocs.SelectedItem as Document;
            if (doc != null)
            {
                _luceneEngine.AddDocument(doc);
            }
        }
    }
}
