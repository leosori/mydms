﻿// -----------------------------------------------------------------------
// <copyright file="UITraceListener.cs">
//     Copyright (C) 2015 Stefan Hoffmann - All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Text;
using System.Windows.Controls;

namespace MyDMS
{
    /// <summary>
    /// Displays Trace output in a ContentControl.
    /// </summary>
    public class UITraceListener : TraceListener
    {
        private readonly StringBuilder _buffer = new StringBuilder();
        private readonly ContentControl _control;
        private readonly bool _clearAfterWriteLine;

        /// <summary>
        /// Creates a new instance of an UITraceListener.
        /// </summary>
        /// <param name="control">The ContentControl to display the messages.</param>
        /// <param name="clearAfterWriteLine">If true, only text starting from the last call to WriteLine will be displayed. The default is false.</param>
        public UITraceListener(ContentControl control, bool clearAfterWriteLine = false)
        {
            _control = control;
            _clearAfterWriteLine = clearAfterWriteLine;
        }

        public override void Write(string message)
        {
            _buffer.Append(message);
            _control.Dispatcher.Invoke(() => _control.Content = _buffer.ToString());
        }

        public override void WriteLine(string message)
        {
            _buffer.Append(message);
            _control.Dispatcher.Invoke(() => _control.Content = _buffer.ToString());
            if (_clearAfterWriteLine)
            {
                _buffer.Clear();
            }
            else
            {
                _buffer.Append(Environment.NewLine);
            }
        }
    }
}