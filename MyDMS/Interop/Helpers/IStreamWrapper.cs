﻿// -----------------------------------------------------------------------
// <copyright file="IStreamWrapper.cs">
//     Copyright (C) 2015 Stefan Hoffmann - All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace MyDMS.Interop.Helpers
{
    /// <summary>
    /// Wraps a Stream and provides a readonly IStream.
    /// </summary>
    public class IStreamWrapper : IStream, IDisposable
    {
        private readonly Stream _stream;
        private bool _disposed = false;

        public IStreamWrapper(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException();
            _stream = stream;
        }

        #region IStream methods

        public void Clone(out IStream ppstm)
        {
            throw new NotImplementedException();
        }

        public void Commit(int grfCommitFlags)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(IStream pstm, long cb, IntPtr pcbRead, IntPtr pcbWritten)
        {
            throw new NotImplementedException();
        }

        public void LockRegion(long libOffset, long cb, int dwLockType)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reads a specified number of bytes from the stream object into memory starting at the current seek pointer.
        /// </summary>
        /// <param name="pv">A pointer to the buffer which the stream data is read into.</param>
        /// <param name="cb">The number of bytes of data to read from the stream object.</param>
        /// <param name="pcbRead">A pointer to a ULONG variable that receives the actual number of bytes read from the stream object.</param>
        public void Read(byte[] pv, int cb, IntPtr pcbRead)
        {
            Marshal.WriteInt32(pcbRead, _stream.Read(pv, 0, cb));
        }

        public void Revert()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Changes the seek pointer to a new location relative to the beginning of the stream, the end of the stream, or the current seek pointer.
        /// </summary>
        /// <param name="dlibMove">The displacement to be added to the location indicated by the dwOrigin parameter. If dwOrigin is STREAM_SEEK_SET, this is interpreted as an unsigned value rather than a signed value.</param>
        /// <param name="dwOrigin">The origin for the displacement specified in dlibMove. The origin can be the beginning of the file (STREAM_SEEK_SET), the current seek pointer (STREAM_SEEK_CUR), or the end of the file (STREAM_SEEK_END).</param>
        /// <param name="plibNewPosition">A pointer to the location where this method writes the value of the new seek pointer from the beginning of the stream. You can set this pointer to NULL. In this case, this method does not provide the new seek pointer.</param>
        public void Seek(long dlibMove, int dwOrigin, IntPtr plibNewPosition)
        {
            long newPos = _stream.Seek(dlibMove, (SeekOrigin)dwOrigin);
            if (plibNewPosition != IntPtr.Zero) {
                Marshal.WriteInt64(plibNewPosition, newPos);
            } 
        }

        public void SetSize(long libNewSize)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieves the STATSTG structure for this stream.
        /// </summary>
        /// <param name="pstatstg">Pointer to a STATSTG structure where this method places information about this stream object.</param>
        /// <param name="grfStatFlag">Specifies that this method does not return some of the members in the STATSTG structure, thus saving a memory allocation operation. Values are taken from the STATFLAG enumeration.</param>
        public void Stat(out System.Runtime.InteropServices.ComTypes.STATSTG pstatstg, int grfStatFlag) {
            pstatstg = new System.Runtime.InteropServices.ComTypes.STATSTG();
            pstatstg.type = 2; //STGTY_STREAM: Indicates that the storage element is a stream object.
            pstatstg.cbSize = _stream.Length;

            if (_stream.CanRead)
            {
                pstatstg.grfMode = _stream.CanWrite ? (int) STGM.READWRITE : (int) STGM.READ;
            }
            else if (_stream.CanWrite)
            {
                pstatstg.grfMode = (int) STGM.WRITE;
            }
            else
            {
                // should never happen
                throw new IOException("Can't read nor write stream");
            }
        }

        public void UnlockRegion(long libOffset, long cb, int dwLockType)
        {
            throw new NotImplementedException();
        }

        public void Write(byte[] pv, int cb, System.IntPtr pcbWritten)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region disposable pattern methods

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            
            if (disposing) {
                _stream.Dispose();
            }

            _disposed = true;
        }

        ~IStreamWrapper()
        {
            Dispose(false);
        }

        #endregion

    }
}
