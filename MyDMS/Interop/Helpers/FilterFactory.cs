﻿// -----------------------------------------------------------------------
// <copyright file="FilterFactory.cs">
//     Copyright (C) 2015 Stefan Hoffmann - All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Threading;
using Microsoft.Win32;
using MyDMS.Interop.Filter;

namespace MyDMS.Interop.Helpers {
    class FilterFactory {
        private const string IFilterGUID = "{89BCB740-6119-101A-BCB7-00DD010655AF}";

        /// <summary>
        /// Retrieves the registered IFilter Guid for the specified extension from the registry.
        /// </summary>
        /// <param name="extension">The file extension, e.g., ".pdf"</param>
        /// <returns>The Guid of the registered IFilter or Guid.Empty if none is found</returns>
        private static Guid GetFilterGuidByExtension(string extension)
        {
            RegistryKey ph = Registry.ClassesRoot.OpenSubKey(extension + "\\PersistentHandler");
            if (ph == null) return Guid.Empty;

            string phGuid = ph.GetValue(null) as string;
            if (phGuid == null) return Guid.Empty;

            RegistryKey filterKey =
                Registry.ClassesRoot.OpenSubKey("CLSID\\" + phGuid + "\\PersistentAddinsRegistered\\" + IFilterGUID);
            if (filterKey == null) return Guid.Empty;

            string filterGuid = filterKey.GetValue(null) as string;
            return filterGuid != null ? new Guid(filterGuid) : Guid.Empty;
        }

        /// <summary>
        /// Loads the IFilter specified by the Guid.
        /// </summary>
        /// <param name="guid">The Guid of the desired IFilter.</param>
        /// <returns>An instance of the IFilter or null.</returns>
        public static IFilter LoadIFilterByGuid(Guid guid)
        {
            Debug.Assert(Thread.CurrentThread.GetApartmentState() == ApartmentState.STA, "FilterFactory nicht innerhalb eines STA Threads");

            Type t = Type.GetTypeFromCLSID(guid);
            return Activator.CreateInstance(t) as IFilter;
        }

        /// <summary>
        /// Loads the IFilter specified by the file extension.
        /// </summary>
        /// <param name="extension">The file extension, e.g., ".pdf"</param>
        /// <returns>An instance of the IFilter or null.</returns>
        public static IFilter LoadIFilterByExtension(string extension)
        {
            Guid fGuid = GetFilterGuidByExtension(extension);
            return fGuid.Equals(Guid.Empty) ? null : LoadIFilterByGuid(fGuid);
        }
    }
}
