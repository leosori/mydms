﻿using System;
using System.Runtime.InteropServices;

namespace MyDMS.Interop.Filter
{
    [StructLayout(LayoutKind.Sequential)]
    public struct FULLPROPSPEC {
        public Guid guidPropSet;
        public PROPSPEC psProperty;
    }
}