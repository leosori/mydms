﻿using System.Runtime.InteropServices;

namespace MyDMS.Interop.Filter
{
    [StructLayout(LayoutKind.Sequential)]
    public struct FILTERREGION {
        public uint idChunk;
        public uint cwcStart;
        public uint cwcExtent;
    }
}