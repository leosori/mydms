using System;
using System.Runtime.InteropServices;
using System.Text;

namespace MyDMS.Interop.Filter
{
    /// <summary>
    /// Scans documents for text and properties (also called attributes).
    /// It extracts chunks of text from these documents, filtering out embedded formatting and retaining information about the position of the text.
    /// It also extracts chunks of values, which are properties of an entire document or of well-defined parts of a document.
    /// IFilter provides the foundation for building higher-level applications such as document indexers and application-independent viewers.
    /// </summary>
    [ComImport]
    [Guid("89BCB740-6119-101A-BCB7-00DD010655AF")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IFilter {
        /// <summary>
        /// Initializes a filtering session.
        /// </summary>
        /// <param name="grfFlags">Values from the IFILTER_INIT enumeration for controlling text standardization, property output, embedding scope, and IFilter access patterns.</param>
        /// <param name="cAttributes">The size of the attributes array. When nonzero, cAttributes takes precedence over attributes specified in grfFlags. If no attribute flags are specified and cAttributes is zero, the default is given by the PSGUID_STORAGE storage property set, which contains the date and time of the last write to the file, size, and so on; and by the PID_STG_CONTENTS 'contents' property, which maps to the main contents of the file.</param>
        /// <param name="aAttributes">Pointer to an array of FULLPROPSPEC structures for the requested properties. When cAttributes is nonzero, only the properties in aAttributes are returned. </param>
        /// <param name="pdwFlags">Information about additional properties available to the caller; from the IFILTER_FLAGS enumeration. </param>
        void Init([MarshalAs(UnmanagedType.U4)] IFILTER_INIT grfFlags, uint cAttributes, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] FULLPROPSPEC[] aAttributes, out uint pdwFlags);

        /// <summary>
        /// Positions filter at beginning of first or next chunk and returns a descriptor.
        /// </summary>
        /// <param name="pStat">A pointer to a STAT_CHUNK structure containing a description of the current chunk.</param>
        /// <returns>This method can return one of these values: S_OK, FILTER_E_END_OF_CHUNKS, FILTER_E_EMBEDDING_UNAVAILABLE, FILTER_E_LINK_UNAVAILABLE, FILTER_E_PASSWORD or FILTER_E_ACCESS</returns>
        [PreserveSig]
        IFilterReturnCodes GetChunk(out STAT_CHUNK pStat);

        /// <summary>
        /// Retrieves text from the current chunk.
        /// </summary>
        /// <param name="pcwcBuffer">On entry, the size of awcBuffer array in wide/Unicode characters. On exit, the number of Unicode characters written to awcBuffer.</param>
        /// <param name="buffer">Text retrieved from the current chunk. Do not terminate the buffer with a character. Use a null-terminated string. The null-terminated string should not exceed the size of the destination buffer.</param>
        /// <returns>This method can return one of these values: S_OK, FILTER_E_NO_TEXT, FILTER_E_NO_MORE_TEXT or FILTER_S_LAST_TEXT</returns>
        /// <remarks>If the current chunk is too large for the awcBuffer array, more than one call to the GetText method can be required to retrieve all the text in the current chunk. Each call to the GetText method retrieves text that immediately follows the text from the last call to the GetText method. The last character from one call can be in the middle of a word, and the first character in the next call would continue that word. Search engines must handle this situation.</remarks>
        [PreserveSig]
        IFilterReturnCodes GetText(ref uint pcwcBuffer, [Out(), MarshalAs(UnmanagedType.LPWStr)] StringBuilder buffer);

        /// <summary>
        /// Retrieves values from the current chunk.
        /// </summary>
        /// <param name="ppPropValue">A pointer to an output variable that receives a pointer to the PROPVARIANT structure that contains the value-type property. </param>
        /// <returns>This method can return one of these values: S_OK, FILTER_E_NO_MORE_VALUES or FILTER_E_NO_VALUES</returns>
        [PreserveSig]
        IFilterReturnCodes GetValue(out UIntPtr ppPropValue);
        
        /// <summary>
        /// Retrieves an interface representing the specified portion of object. Currently reserved for future use.
        /// </summary>
        /// <param name="origPos">A FILTERREGION structure that contains the position of the text.</param>
        /// <param name="riid">A reference to the requested interface identifier.</param>
        /// <param name="ppunk">A pointer to a variable that receives the interface pointer requested in riid. Upon successful return, *ppunk contains the requested interface pointer.</param>
        void BindRegion([MarshalAs(UnmanagedType.Struct)] FILTERREGION origPos, ref Guid riid, out UIntPtr ppunk);
    }

    /// <summary>
    /// IFilter return codes
    /// </summary>
    public enum IFilterReturnCodes : uint {
        /// <summary>
        /// The operation was completed successfully.
        /// </summary>
        S_OK = 0,
        /// 
        /// The function was denied access to the filter file. 
        /// 
        E_ACCESSDENIED = 0x80070005,
        /// 
        /// The function encountered an invalid handle, probably due to a low-memory situation. 
        /// 
        E_HANDLE = 0x80070006,
        /// <summary>
        /// Count and contents of attributes do not agree.
        /// </summary>
        E_INVALIDARG = 0x80070057,
        /// 
        /// Out of memory
        /// 
        E_OUTOFMEMORY = 0x8007000E,
        /// <summary>
        /// This method is not currently implemented.
        /// </summary>
        E_NOTIMPL = 0x80004001,
        /// <summary>
        /// File to filter was not previously loaded.
        /// </summary>
        E_FAIL = 0x80000008,
        /// <summary>
        /// Access has been denied because of password protection or similar security measures.
        /// </summary>
        FILTER_E_PASSWORD = 0x8004170B,
        /// 
        /// The document format is not recognised by the filter
        /// 
        FILTER_E_UNKNOWNFORMAT = 0x8004170C,
        /// <summary>
        /// The flags member of the STAT_CHUNK structure for the current chunk does not have a value of CHUNK_TEXT. 
        /// </summary>
        FILTER_E_NO_TEXT = 0x80041705,
        /// <summary>
        /// The previous chunk is the last chunk.
        /// </summary>
        FILTER_E_END_OF_CHUNKS = 0x80041700,
        /// <summary>
        /// All the text in the current chunk has been returned. Additional calls to the GetText method should return this error until the IFilter::GetChunk method has been called successfully. 
        /// </summary>
        FILTER_E_NO_MORE_TEXT = 0x80041701,
        /// <summary>
        /// The GetValue method has already been called on this chunk; this value should be returned until the IFilter::GetChunk method has been called successfully and has advanced to the next chunk.
        /// </summary>
        FILTER_E_NO_MORE_VALUES = 0x80041702,
        /// <summary>
        /// General access failure.
        /// </summary>
        FILTER_E_ACCESS = 0x80041703,
        /// 
        /// Moniker doesn't cover entire region
        /// 
        FILTER_W_MONIKER_CLIPPED = 0x00041704,
        /// <summary>
        /// The current chunk does not have a CHUNKSTATE enumeration value of CHUNK_VALUE. 
        /// </summary>
        FILTER_E_NO_VALUES = 0x80041706,
        /// <summary>
        /// The next chunk is an embedding and no content filter is available.
        /// </summary>
        FILTER_E_EMBEDDING_UNAVAILABLE = 0x80041707,
        /// <summary>
        /// The next chunk is a link and no content filter is available.
        /// </summary>
        FILTER_E_LINK_UNAVAILABLE = 0x80041708,
        /// <summary>
        /// As an optimization, the last call that returns text can return FILTER_S_LAST_TEXT, indicating that the next call to the GetText method will return FILTER_E_NO_MORE_TEXT. This optimization can save time by eliminating unnecessary calls to GetText.
        /// </summary>
        FILTER_S_LAST_TEXT = 0x00041709,
        /// 
        /// This is the last value in the current chunk
        /// 
        FILTER_S_LAST_VALUES = 0x0004170A
    }
}