using System;
using System.Runtime.InteropServices;

namespace MyDMS.Interop.Filter
{
    [StructLayout(LayoutKind.Explicit)]
    public struct PROPSPEC {
        [FieldOffset(0)]
        public uint ulKind;
        [FieldOffset(4)]
        public uint propid;
        [FieldOffset(4)]
        public IntPtr lpwstr;
    }
}