using System.Runtime.InteropServices;

namespace MyDMS.Interop.Filter
{
    /// <summary>
    /// Describes the characteristics of a chunk.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct STAT_CHUNK {
        /// <summary>
        /// The chunk identifier. 
        /// Chunk identifiers must be unique for the current instance of the IFilter interface. Chunk identifiers must be in ascending order. 
        /// The order in which chunks are numbered should correspond to the order in which they appear in the source document. 
        /// Some search engines can take advantage of the proximity of chunks of various properties. 
        /// If so, the order in which chunks with different properties are emitted will be important to the search engine.
        /// </summary>
        public uint idChunk;
        /// <summary>
        /// The type of break that separates the previous chunk from the current chunk. Values are from the CHUNK_BREAKTYPE enumeration.
        /// </summary>
        [MarshalAs(UnmanagedType.U4)]
        public CHUNK_BREAKTYPE breakType;
        /// <summary>
        /// Indicates whether this chunk contains a text-type or a value-type property. Flag values are taken from the CHUNKSTATE enumeration. 
        /// If the CHUNK_TEXT flag is set, IFilter::GetText should be used to retrieve the contents of the chunk as a series of words. 
        /// If the CHUNK_VALUE flag is set, IFilter::GetValue should be used to retrieve the value and treat it as a single property value. 
        /// If the filter dictates that the same content be treated as both text and as a value, the chunk should be emitted twice in two different chunks, each with one flag set.
        /// </summary>
        [MarshalAs(UnmanagedType.U4)]
        public CHUNKSTATE flags;
        /// <summary>
        /// The language and sublanguage associated with a chunk of text. Chunk locale is used by document indexers to perform proper word breaking of text. 
        /// If the chunk is neither text-type nor a value-type with data type VT_LPWSTR, VT_LPSTR or VT_BSTR, this field is ignored.
        /// </summary>
        public uint locale;
        /// <summary>
        /// The property to be applied to the chunk. See FULLPROPSPEC. If a filter requires that the same text have more than one property, it needs to emit the text once for each property in separate chunks.
        /// </summary>
        [MarshalAs(UnmanagedType.Struct)]
        public FULLPROPSPEC attribute;
        /// <summary>
        /// The ID of the source of a chunk. The value of the idChunkSource member depends on the nature of the chunk:
        /// If the chunk is a text-type property, the value of the idChunkSource member must be the same as the value of the idChunk member.
        /// <list type="bullet">
        /// <item><description>If the chunk is a text-type property, the value of the idChunkSource member must be the same as the value of the idChunk member.</description></item>
        /// <item><description>If the chunk is an internal value-type property derived from textual content, the value of the idChunkSource member is the chunk ID for the text-type chunk from which it is derived.</description></item>
        /// <item><description>If the filter attributes specify to return only internal value-type properties, there is no content chunk from which to derive the current internal value-type property. In this case, the value of the idChunkSource member must be set to zero, which is an invalid chunk.</description></item>
        /// </list>
        /// </summary>
        public uint idChunkSource;
        /// <summary>
        /// The offset from which the source text for a derived chunk starts in the source chunk.
        /// </summary>
        public uint cwcStartSource;
        /// <summary>
        /// The length in characters of the source text from which the current chunk was derived. 
        /// A zero value signifies character-by-character correspondence between the source text and the derived text. A nonzero value means that no such direct correspondence exists.
        /// </summary>
        public uint cwcLenSource;
    }
}