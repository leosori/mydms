﻿// -----------------------------------------------------------------------
// <copyright file="LuceneEngine.cs">
//     Copyright (C) 2015 Stefan Hoffmann - All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using LuceneDocument = Lucene.Net.Documents.Document;
using Version = Lucene.Net.Util.Version;

namespace MyDMS.SearchEngine
{
    public class LuceneEngine
    {
        private Directory _directory;
        private Analyzer _analyzer;
        private IndexWriter _writer;
        private const Version _matchVersion = Version.LUCENE_30;

        /// <summary>
        /// Initializes Lucene with a temporary in-memory index.
        /// </summary>
        public LuceneEngine()
        {
            _directory = new RAMDirectory();
            _analyzer = new StandardAnalyzer(_matchVersion);
            _writer = new IndexWriter(_directory, _analyzer, IndexWriter.MaxFieldLength.UNLIMITED);
        }

        #region Mapping methods between Lucene Documents and DMS Documents

        private static LuceneDocument MapDocumentToLuceneDocument(Document doc)
        {
            var ldoc = new LuceneDocument();

            //doc.Add(new Field("Id", sampleData.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            ldoc.Add(new Field("Name", doc.Name, Field.Store.YES, Field.Index.ANALYZED));
            ldoc.Add(new Field("Content", doc.Content, Field.Store.YES, Field.Index.ANALYZED));
            ldoc.Add(new Field("Filename", doc.Filename, Field.Store.YES, Field.Index.NOT_ANALYZED));
            ldoc.Add(new Field("Tag", doc.Tag, Field.Store.YES, Field.Index.ANALYZED));
            ldoc.Add(new Field("ModifiedDate", DateTools.DateToString(doc.ModifiedDate,DateTools.Resolution.SECOND), Field.Store.YES, Field.Index.NOT_ANALYZED));
            if (doc.DocumentDate != null)
            {
                ldoc.Add(new Field("DocumentDate", DateTools.DateToString(doc.DocumentDate.Value,DateTools.Resolution.DAY),Field.Store.YES, Field.Index.NOT_ANALYZED));
            }

            return ldoc;
        }

        private static Document MapLuceneDocumentToDocument(LuceneDocument ldoc)
        {
            var doc = new Document
            {
                Name = ldoc.Get("Name"),
                Content = ldoc.Get("Content"),
                Filename = ldoc.Get("Filename"),
                Tag = ldoc.Get("Tag"),
                ModifiedDate = DateTools.StringToDate(ldoc.Get("ModifiedDate"))
            };
            var date = ldoc.Get("DocumentDate");
            if (date != null)
            {
                doc.DocumentDate = DateTools.StringToDate(date);
            }

            return doc;
        }

        private static IEnumerable<Document> MapLuceneDocumentsToDocuments(IEnumerable<LuceneDocument> lDocs) {
            return lDocs.Select(MapLuceneDocumentToDocument).ToList();
        }

        private static IEnumerable<Document> MapSearchResultsToDocuments(IEnumerable<ScoreDoc> hits, IndexSearcher searcher) {
            return hits.Select(hit => MapLuceneDocumentToDocument(searcher.Doc(hit.Doc))).ToList();
        }

        #endregion

        /// <summary>
        /// Add a Document to the index. If a Document with the same filename already exists, it will be replaced.
        /// </summary>
        /// <param name="doc">The Document.</param>
        public void AddDocument(Document doc)
        {
            _writer.UpdateDocument(new Term("Filename", doc.Filename), MapDocumentToLuceneDocument(doc));
            _writer.Commit();
        }

        /// <summary>
        /// Adds a collection of Documents to the index. If a Document with the same filename already exists, it will be replaced.
        /// </summary>
        /// <param name="docs">The Documents collection.</param>
        public void AddDocuments(IEnumerable<Document> docs)
        {
            foreach (var doc in docs)
            {
                _writer.UpdateDocument(new Term("Filename", doc.Filename), MapDocumentToLuceneDocument(doc));
            }
            _writer.Commit();
        }

        private IEnumerable<Document> ExecuteSearch(string searchQuery, string searchField = "")
        {
            // ignore wildcard-only search
            if (string.IsNullOrEmpty(searchQuery.Replace("*", "").Replace("?", ""))) return new List<Document>();

            using (var searcher = new IndexSearcher(_directory, true))
            {
                const int hitsLimit = 100;

                QueryParser parser = !string.IsNullOrEmpty(searchField) ? 
                    new QueryParser(_matchVersion, searchField, _analyzer) : 
                    new MultiFieldQueryParser(_matchVersion, new[] {"Name", "Filename", "Tag", "Content"}, _analyzer);
                
                var query = parser.Parse(searchQuery.Trim());
                var hits = searcher.Search(query, null, hitsLimit).ScoreDocs;
                var results = MapSearchResultsToDocuments(hits, searcher);

                return results;
            }
        }

        /// <summary>
        /// Perform a search for all words beginning with the specified terms.
        /// Does NOT support advanced query syntax.
        /// </summary>
        /// <param name="input">The query.</param>
        /// <param name="fieldName">The default field name. If left empty, several fields will be searched.</param>
        /// <returns>All matching Documents.</returns>
        public IEnumerable<Document> Search(string input, string fieldName = "")
        {
            if (string.IsNullOrEmpty(input)) return new List<Document>();

            // remove hyphens and append an asterisk to every term
            var terms = input.Trim().Replace("-", " ").Split(new[]{' '}, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Trim() + "*");
            input = string.Join(" ", terms);

            return ExecuteSearch(input, fieldName);
        }

        /// <summary>
        /// Perform a search without any modification of the query string.
        /// </summary>
        /// <param name="input">The query.</param>
        /// <param name="fieldName">The default field name. If left empty, several fields will be searched.</param>
        /// <returns>All matching Documents.</returns>
        public IEnumerable<Document> RawSearch(string input, string fieldName = "")
        {
            return string.IsNullOrEmpty(input) ? new List<Document>() : ExecuteSearch(input, fieldName);
        }

        /// <summary>
        /// Returns all Documents currently indexed.
        /// </summary>
        /// <returns>All indexed Documents.</returns>
        public IEnumerable<Document> GetAllIndexedDocuments()
        {
            using (var searcher = new IndexSearcher(_directory, true))
            {
                var docs = new List<LuceneDocument>();
                var term = searcher.IndexReader.TermDocs();
                while (term.Next())
                {
                    docs.Add(searcher.Doc(term.Doc));
                }
                return MapLuceneDocumentsToDocuments(docs);
            }
        }

        /// <summary>
        /// Checks if a Document with the specified filename is indexed.
        /// </summary>
        /// <param name="filename">The filename of the Document.</param>
        /// <returns>True if the index contains the Document. False otherwise.</returns>
        public bool ContainsDocument(string filename)
        {
            using (var searcher = new IndexSearcher(_directory, true))
            {
                const int hitsLimit = 100;
                var searchQuery = new TermQuery(new Term("Filename", filename));
                
                var hits = searcher.Search(searchQuery, null, hitsLimit);
                Debug.WriteLine("File '" + filename + "' is " + hits.TotalHits + " times in the index");
                return hits.TotalHits > 0;
            }
        }

        /// <summary>
        /// Checks a list of filenames and returns all files that are not indexed.
        /// </summary>
        /// <param name="files">The filenames.</param>
        /// <returns>All files not currently indexed.</returns>
        public IList<string> GetNonIndexedFiles(IEnumerable<string> files)
        {
            const int hitsLimit = 100;
            using (var searcher = new IndexSearcher(_directory, true))
            {
                return files.Where(file =>
                {
                    var searchQuery = new TermQuery(new Term("Filename", file));
                    var hits = searcher.Search(searchQuery, null, hitsLimit);
                    return hits.TotalHits == 0;
                }).ToList();
            }
        }

        public Document GetDocument(string filename)
        {
            using (var searcher = new IndexSearcher(_directory, true)) {
                const int hitsLimit = 100;
                var searchQuery = new TermQuery(new Term("Filename", filename));

                var hits = searcher.Search(searchQuery, null, hitsLimit);
                Debug.Assert(hits.TotalHits <= 1);
                return hits.TotalHits == 1 ? MapLuceneDocumentToDocument(searcher.Doc(hits.ScoreDocs[0].Doc)) : null;
            }
        }
    }
}