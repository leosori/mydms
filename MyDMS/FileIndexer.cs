﻿// -----------------------------------------------------------------------
// <copyright file="FileIndexer.cs">
//     Copyright (C) 2015 Stefan Hoffmann - All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MyDMS.Interop;
using MyDMS.Interop.Filter;
using MyDMS.Interop.Helpers;

namespace MyDMS
{
    public class FileIndexer
    {
        private static string ExtractTextFromFileSTA(string filename)
        {
            StringBuilder result = new StringBuilder();
            IFilter filter = null;

            Debug.Assert(Thread.CurrentThread.GetApartmentState() == ApartmentState.STA, "FileIndexer nicht innerhalb eines STA Threads");

            try
            {
                filter = LoadIFilterWithFile(filename);

                uint i;
                STAT_CHUNK ps;
                const IFILTER_INIT iflags = IFILTER_INIT.CANON_HYPHENS |
                                            IFILTER_INIT.CANON_PARAGRAPHS |
                                            IFILTER_INIT.CANON_SPACES |
                                            IFILTER_INIT.APPLY_CRAWL_ATTRIBUTES |
                                            IFILTER_INIT.APPLY_INDEX_ATTRIBUTES |
                                            IFILTER_INIT.APPLY_OTHER_ATTRIBUTES |
                                            IFILTER_INIT.HARD_LINE_BREAKS |
                                            IFILTER_INIT.SEARCH_LINKS |
                                            IFILTER_INIT.FILTER_OWNED_VALUE_OK |
                                            IFILTER_INIT.INDEXING_ONLY;

                filter.Init(iflags, 0, null, out i);

                while (filter.GetChunk(out ps) == IFilterReturnCodes.S_OK)
                {
                    switch (ps.breakType)
                    {
                        case CHUNK_BREAKTYPE.CHUNK_NO_BREAK:
                            break;
                        case CHUNK_BREAKTYPE.CHUNK_EOW:
                        case CHUNK_BREAKTYPE.CHUNK_EOS:
                            result.Append(" ");
                            break;
                        case CHUNK_BREAKTYPE.CHUNK_EOP:
                        case CHUNK_BREAKTYPE.CHUNK_EOC:
                            result.AppendLine();
                            break;
                    }

                    if (ps.flags != CHUNKSTATE.CHUNK_TEXT) continue;

                    result.Append(GetTextFromChunk(filter));
                }
            }
            catch (Exception e)
            {
                return e.Message; //TODO Exception werfen oder leeren String zurückgeben
            }
            finally
            {
                if (filter != null)
                {
                    Marshal.ReleaseComObject(filter);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }

            return result.ToString();
        }

        public static string ExtractTextFromFile(string filename)
        {
            if (Thread.CurrentThread.GetApartmentState() != ApartmentState.STA)
            {
                return StaTask.Run(() => ExtractTextFromFileSTA(filename)).Result;
            }
            return ExtractTextFromFileSTA(filename);
        }

        public static Task<string> ExtractTextFromFileAsync(string filename)
        {
            return StaTask.Run(() => ExtractTextFromFileSTA(filename));
        }

        private static IFilter LoadIFilterWithFile(string filename)
        {
            IFilter filter = FilterFactory.LoadIFilterByExtension(Path.GetExtension(filename));
            if (filter == null)
            {
                throw new Exception("No IFilter for '" + Path.GetExtension(filename) + "' found");
            }

            // IPersistsStream is the more common interface for IFilters, so lets try it first
            IPersistStream pStreamFilter = filter as IPersistStream;

            if (pStreamFilter != null)
            {
                IStreamWrapper stream = new IStreamWrapper(new FileStream(filename, FileMode.Open, FileAccess.Read));
                pStreamFilter.Load(stream);
            }
            else
            {
                IInitializeWithStream streamFilter = filter as IInitializeWithStream;
                IInitializeWithFile fileFilter = filter as IInitializeWithFile;
                if (streamFilter != null)
                {
                    IStreamWrapper stream = new IStreamWrapper(new FileStream(filename, FileMode.Open, FileAccess.Read));
                    streamFilter.Initialize(stream, STGM.READ);
                }
                else if (fileFilter != null)
                {
                    fileFilter.Initialize(filename, STGM.READ);
                }
                else
                {
                    throw new Exception("Could not initialize the IFilter for '" + Path.GetExtension(filename) + "'");
                }
            }
            return filter;
        }

        private static string GetTextFromChunk(IFilter filter)
        {
            uint bufferSize = 65536;
            StringBuilder buffer = new StringBuilder((int) bufferSize);
            StringBuilder result = new StringBuilder();

            bool hasMoreText = true;
            while (hasMoreText)
            {
                bufferSize = (uint) buffer.Capacity;
                IFilterReturnCodes scode = filter.GetText(ref bufferSize, buffer);

                switch (scode)
                {
                    case IFilterReturnCodes.S_OK:
                        if (bufferSize > 0 && buffer.Length > 0)
                        {
                            // Some IFilters report sbBuffer.Length+1 as pcwcBuffer
                            // Could be related to string termination or end-of-line chars
                            if (buffer.Length < bufferSize)
                            {
                                bufferSize = (uint) buffer.Length;
                            }

                            // The StringBuilder can contain old stuff beyond the size specified by pcwcBuffer
                            // This depends on the implementation of the IFilter and how it uses the provided buffer.
                            result.Append(buffer.ToString(0, (int) bufferSize));
                        }
                        break;

                    case IFilterReturnCodes.FILTER_S_LAST_TEXT:
                        hasMoreText = false;
                        goto case IFilterReturnCodes.S_OK;

                    case IFilterReturnCodes.FILTER_E_NO_MORE_TEXT:
                        hasMoreText = false;
                        break;

                    case IFilterReturnCodes.FILTER_E_NO_TEXT:
                        return String.Empty; //throw exception?

                    default:
                        // should never happen
                        Marshal.ThrowExceptionForHR((int) scode);
                        break;
                }
            }

            return result.ToString();
        }
    }
}